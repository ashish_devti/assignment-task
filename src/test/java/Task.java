import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Task {
    public WebDriver driver;

    public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile = new File(fileWithPath);
        FileUtils.copyFile(SrcFile, DestFile);

    }

    @BeforeTest
    public void openBrowser() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://automationpractice.com/index.php");

    }

    @Test(priority = 1)
    public void login() throws Exception {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//a[normalize-space()='Sign in']")).click();
        driver.findElement(By.xpath("//input[@id='email']")).sendKeys("ashishdevtiwari@gmail.com");
        driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys("Test@123");
        driver.findElement(By.xpath("//span[normalize-space()='Sign in']")).click();
    }

    @Test(priority = 2)
    public void addAddress() {
        driver.findElement(By.xpath("//span[normalize-space()='My addresses']")).click();
        driver.findElement(By.xpath("//span[normalize-space()='Add a new address']")).click();
        driver.findElement(By.xpath("//input[@id='address1']")).sendKeys("A");
        driver.findElement(By.xpath("//input[@id='address2']")).sendKeys("B");
        driver.findElement(By.xpath("//input[@id='city']")).sendKeys("C");

        Select state = new Select(driver.findElement(By.xpath("//select[@id='id_state']")));
        state.selectByVisibleText("Florida");

        driver.findElement(By.xpath("//input[@id='postcode']")).sendKeys("86007");
        driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("1234");
        driver.findElement(By.xpath("//input[@id='alias']")).clear();
        driver.findElement(By.xpath("//input[@id='alias']")).sendKeys("My Add2");
        driver.findElement(By.xpath("//span[normalize-space()='Save']")).click();

    }

    @Test(priority = 3)
    public void goToSummerDressForWomen() {

        Actions actions = new Actions(driver);
        WebElement menuOption = driver.findElement(By.xpath("//a[@title='Women']"));
        actions.moveToElement(menuOption).perform();

        driver.findElement(By.xpath("//li[@class='sfHover']//a[@title='Summer Dresses'][normalize-space()='Summer Dresses']")).click();

        driver.findElement(By.xpath("//i[@class='icon-th-list']")).click();
    }

    @Test(priority = 4)
    public void shoppingOne() {
        driver.findElement(By.xpath("//li[@class='ajax_block_product first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line last-mobile-line col-xs-12']//a[@title='Printed Summer Dress'][normalize-space()='Printed Summer Dress']")).click();


        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).clear();
        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).sendKeys("5");

        Select size = new Select(driver.findElement(By.xpath("//select[@id='group_1']")));
        size.selectByVisibleText("L");

        driver.findElement(By.xpath("//a[@id='color_11']")).click();

        driver.findElement(By.xpath("//span[normalize-space()='Add to cart']")).click();

        driver.findElement(By.xpath("//span[@title='Continue shopping']//span[1]")).click();
    }

    @Test(priority = 5)
    public void shoppingTwo() {
        driver.findElement(By.xpath("//div[@class='breadcrumb clearfix']//a[@title='Summer Dresses'][normalize-space()='Summer Dresses']")).click();
        driver.findElement(By.xpath("//li[@class='ajax_block_product last-line last-item-of-tablet-line last-mobile-line col-xs-12']//a[@title='Printed Summer Dress'][normalize-space()='Printed Summer Dress']")).click();

        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).clear();
        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).sendKeys("5");

        Select size2 = new Select(driver.findElement(By.xpath("//select[@id='group_1']")));
        size2.selectByVisibleText("L");

        driver.findElement(By.xpath("//a[@id='color_8']")).click();

        driver.findElement(By.xpath("//span[normalize-space()='Add to cart']")).click();

        driver.findElement(By.xpath("//span[@title='Continue shopping']//span[1]")).click();
    }

    //3rd shopping
    @Test(priority = 6)
    public void shoppingThree() {

        driver.findElement(By.xpath("//div[@class='breadcrumb clearfix']//a[@title='Summer Dresses'][normalize-space()='Summer Dresses']")).click();
        driver.findElement(By.xpath("//h5[@itemprop='name']//a[@title='Printed Chiffon Dress'][normalize-space()='Printed Chiffon Dress']")).click();


        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).clear();
        driver.findElement(By.xpath("//input[@id='quantity_wanted']")).sendKeys("5");

        Select size3 = new Select(driver.findElement(By.xpath("//select[@id='group_1']")));
        size3.selectByVisibleText("L");

        driver.findElement(By.xpath("//a[@id='color_15']")).click();

        driver.findElement(By.xpath("//span[normalize-space()='Add to cart']")).click();

        driver.findElement(By.xpath("//span[normalize-space()='Proceed to checkout']")).click();
    }


    //payment
    @Test(priority = 7)
    public void goToPayment() throws Exception {

        driver.findElement(By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']//span[contains(text(),'Proceed to checkout')]")).click();
        driver.findElement(By.xpath("//button[@name='processAddress']//span[contains(text(),'Proceed to checkout')]")).click();

        driver.findElement(By.xpath("//input[@id='cgv']")).click();
        driver.findElement(By.xpath("//button[@name='processCarrier']//span[contains(text(),'Proceed to checkout')]")).click();

        driver.findElement(By.xpath("//a[@title='Pay by bank wire']")).click();
        driver.findElement(By.xpath("//span[normalize-space()='I confirm my order']")).click();

        //Move to my account
        driver.findElement(By.xpath("//span[normalize-space()='Ashish Dev']")).click();
        driver.findElement(By.xpath("//span[normalize-space()='Order history and details']")).click();

        //Screenshot
        this.takeSnapShot(driver, "/home/cuelogic.local/ashish.tiwari/invoice.png");
        //logout
        driver.findElement(By.xpath("//a[@title='Log me out']")).click();
    }

    @AfterTest
    public void closeBrowser() {
        driver.close();
    }


}
